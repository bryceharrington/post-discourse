#!/usr/bin/env bash
#
# Copyright (C) 2011-2012 Bryce Harrington <bryce@canonical.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
# Smoke tests the post-discourse cli script itself.

progname=$(basename ${0})

function usage() {
    cat >&2 <<EOF
Usage: ${progname} <test-config.yml>

Runs a set of canned operations against a given Discourse instance.

This requires a test instance of discourse to be available to test
against.  The intent is to be non-destructive of the instance, and to
not require it to be reset or cleaned up between test runs, however it
will be adding content to it, and as always there is risk of something
awkward occurring.  So don't run this against production discourse
servers.

To create an API key in a test server:
 1.  Click on the hamburger menu at top right
 2.  Click 'Admin' in the drop down
 3.  Click 'Users' in the toolbar menu
 4.  Click the username for the user you want to run the test under
 5.  Under Permissions, beside API Key click 'Generate'
 6.  Note the API Key and Username along with the api_base
 7.  Create a config file, with the following contents:

     ### my-discourse-test.yml ###
     username:  <username>
     api_base:  <api_base>
     api_key:   <api_key>

Run the smoketest using this file:

     $ smoketest_post_discourse.sh my-discourse-test.yml

EOF
}

function warn() {
    echo "Warning:  ${*}" 1>&2
}

function die() {
    echo "Error:  ${*}" 1>&2
    exit 1
}

########################################################################
### Setup

config_filename="${1}"
[ -n "${config_filename}" ] \
    || usage 1

# TODO: Locate post-discourse script
post_discourse=$(which post-discourse) \
    || die "Could not find post-discourse.  Is it installed?"

# Set Bash's internal field separator to per-line rather than per-word.
# This is to simplify processing of line-oriented output from post-discourse.
SAVEIFS=${IFS}
IFS=$(echo -en "\n\b")

########################################################################
### General checks

# Check that help works
${post_discourse} --help
echo "---"
${post_discourse} -h
echo "---"

# Check version
${post_discourse} --version
echo "---"
${post_discourse} -V
echo "---"

# Override config file
${post_discourse} --config "${config_filename}" --debug
echo "---"


########################################################################
### Read-only operation checks (no auth needed)

# TODO: Get list of categories
${post_discourse} --api-base ${api_base} --read-only --list-categories
echo "---"

# List existing topics in discourse
${post_discourse} --api-base ${api_base} --read-only --list-topics
echo "---"

# List topics in a particular category
categories=$(${post_discourse} --api-base ${api_base} --read-only --list-categories)
for category in ${categories}; do
    category_id=$(echo ${category} | cut -d' ' -f1)
    category_name=$(echo ${category} | cut -d' ' -f2)

    echo "Topics for '${category_name}' (id ${category_id}):"
    ${post_discourse} --api-base ${api_base} --read-only --list-topics --category ${category_id}
    echo ""
done
echo "---"

# List posts in particular topics
topics=$(${post_discourse} --api-base ${api_base} --read-only --list-topics)
for topic in ${topics}; do
    topic_id=$(echo ${topic} | cut -d' ' -f1)
    topic_name=$(echo ${topic} | cut -d' ' -f2)

    echo "Posts for '${topic_name}' (id ${topic_id}):"
    ${post_discourse} --api-base ${api_base} --read-only --list-posts --topic ${topic_id}
    echo ""
done
echo "---"

########################################################################
### Read-write operations (requires api-key and api-username)

# TODO:
# curl -X GET "http://10.208.83.163:3000/admin/users/list/active.json"

# TODO: Add a post to a topic
${post_discourse} --config "${config_filename}" --topic "${topic_id}" --add-post "Optional title for the post" <<EOF
A post is a post that posts in the post.
I am a yam, mam.
EOF

# TODO: Verify the post is now present

# TODO: Delete the newly added post

# TODO: Verify it is deleted

IFS=${SAVEIFS}
