#!/usr/bin/env python3
# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
#
# Copyright (C) 2019-2020 Bryce Harrington <bryce@canonical.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import re
import sys
import os.path

import pytest

sys.path.insert(0, os.path.realpath(os.path.join(os.path.dirname(__file__), "..")))

from post_discourse.discourse import Discourse

class MockRequestResult(object):
    """A stand-in for a request result."""
    def __init__(self, data):
        self.data = data

    def raise_for_status(self):
        return True

    def json(self):
        # Return a response
        return self.data

@pytest.fixture(scope="module")
def mock_request():
    """Fixture mimicking the requests module functionality

    This provides synthetic get() and post() operations that serve as
    replacements for the requests module's get and post routines,
    respectively.

    Dummy data is included for categories, topics, and posts, that
    replicates Discourse's REST data model.

    This mock also provides convenience routines for keywork arguments
    to pass to the Discourse module to configure it for testing, including
    the test api key and username, and the get/post replacement routines.
    """
    class MockRequest(object):
        USERNAME = 'tester'
        API_BASE = 'http://0.0.0.1:3000'
        API_KEY =  '000a' * 16

        def __init__(self):
            # Default collection of categories, topics, and posts to query
            self._categories = [
                { 'id': 0, 'slug': 'general' },
                { 'id': 1, 'slug': 'specific' },
                ]
            self._topics = [
                { 'id': 0, 'slug': 'topic-a', 'title': 'title-a', 'body': 'body-a' },
                { 'id': 1, 'slug': 'topic-b', 'title': 'title-b', 'body': 'body-b' },
                { 'id': 2, 'slug': 'topic-c', 'title': 'title-c', 'body': 'body-c' },
                ]
            self._posts = [
                { 'id': 0, 'name': 'post-0', 'body': 'body-0', 'locked': False },
                { 'id': 1, 'name': 'post-1', 'body': 'body-1', 'locked': False },
                { 'id': 2, 'name': 'post-2', 'body': 'body-2', 'locked': False },
                { 'id': 3, 'name': 'post-3', 'body': 'body-3', 'locked': False },
                { 'id': 4, 'name': 'post-4', 'body': 'body-4', 'locked': False },
                { 'id': 5, 'name': 'post-5', 'body': 'body-5', 'locked': False },
                ]

        def category_by_id(self, category_id):
            if int(category_id) > len(self._category):
                return None
            return list(filter(lambda i: i['id'] == category_id, self._categories))[0]

        def topic_by_id(self, topic_id):
            if int(topic_id) > len(self._topics):
                return None
            return list(filter(lambda i: i['id'] == topic_id, self._topics))[0]

        def post_by_id(self, post_id):
            if int(post_id) > len(self._posts):
                return None
            return list(filter(lambda i: i['id'] == post_id, self._posts))[0]

        def get(self, url:str):
            data = {}
            if url == self.API_BASE+'/ping':
                data = { 'reply': 'pong' }
            elif url == self.API_BASE+'/categories.json':
                data = {
                    'category_list': {
                        'categories': self._categories
                        }
                    }
            elif (url == self.API_BASE+'/latest.json' or
                  url == self.API_BASE+'/top.json'):
                data = {
                    'topic_list': {
                        'topics': self._topics
                        }
                    }
            elif url == self.API_BASE+'/posts.json':
                data = {
                    'post_stream': {
                        'posts': self._posts
                        }
                    }

            # Get a topic
            m = re.match(r'%s/t/(\d+).json'%(self.API_BASE), url)
            if m:
                data = self._topics[int(m.group(1))]

            # Get a post
            m = re.match(r'%s/posts/(\d+).json'%(self.API_BASE), url)
            if m:
                data = self._posts[int(m.group(1))]

            return MockRequestResult(data)

        def post(self, url:str, data:str, params:dict=None, headers:dict=None):
            # Sanity check POST operations
            m = re.match('%s/ping'%(self.API_BASE), url)
            if m:
                return MockRequestResult({'reply': 'pong'})

            # Lock/unlock posts
            m = re.match(r'%s/posts/(\d+)/locked'%(self.API_BASE), url)
            if m:
                i = int(m.group(1))
                if i > len(self._posts):
                    raise IndexError
                self._posts[i]['locked'] = data['locked']
                return MockRequestResult({'locked': self._posts[i]['locked']})

            # Create topics and posts
            m = re.match(r'%s/posts.json'%(self.API_BASE), url)
            if m:
                if 'topic_id' not in data.keys():
                    # New topic
                    topic_id = len(self._topics) + 1
                    new_topic = {
                        'id': topic_id,
                        'name': data.get('title', ''),
                        'body': data.get('raw', '')
                        }
                    self._topics.append(new_topic)
                    return MockRequestResult(new_topic)

                else:
                    # New post
                    post_id = len(self._posts) + 1
                    new_post = {
                        'id': post_id,
                        'topic_id': data.get('topic_id', '0'),
                        'name': data.get('title', ''),
                        'body': data.get('raw', ''),
                        'locked': False
                        }
                    self._posts.append(new_post)
                    return MockRequestResult(new_post)

            return MockRequestResult({})

        def ro_args(self):
            return {
                'api_base':   self.API_BASE,
                '_get_func':  self.get,
                '_post_func': None
                }

        def rw_args(self):
            return {
                'api_base':   self.API_BASE,
                'api_key':    self.API_KEY,
                'username':   self.USERNAME,
                '_get_func':  self.get,
                '_post_func': self.post
                }

    return MockRequest()

def test_create_readonly(mock_request):
    """Checks discourse object can be created in read-only mode

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    """
    discourse = Discourse(**mock_request.ro_args())
    assert discourse is not None
    assert discourse._get_func is not None
    assert discourse._post_func is None

def test_create_readwrite(mock_request):
    """Checks discourse object can be created in read-write mode

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    """
    discourse = Discourse(**mock_request.rw_args())
    assert discourse is not None
    assert discourse._get_func is not None
    assert discourse._post_func is not None

def test_get_readonly(mock_request):
    """Checks the low level GET routine for read-only operations

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    """
    discourse = Discourse(**mock_request.ro_args())
    r = discourse._get('ping')
    assert r is not None
    assert type(r) is dict
    assert r['reply'] == 'pong'

def test_get_readwrite(mock_request):
    """Checks the low level GET routine for read-write operations

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    """
    discourse = Discourse(**mock_request.rw_args())
    r = discourse._get('ping')
    assert r is not None
    assert type(r) is dict
    assert r['reply'] == 'pong'

def test_post(mock_request):
    """Checks the low level POST routine

    (Not to be confused with test_create_post, which, er, POSTs a post
    object.)

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    """
    discourse = Discourse(**mock_request.rw_args())
    r = discourse._post('ping')
    assert r is not None
    assert type(r) is dict
    assert r['reply'] == 'pong'

@pytest.mark.parametrize('category_id, expected_category_title', [
    (1, 'foobar')
    ])
def test_get_categories(mock_request, category_id, expected_category_title):
    """Checks that categories can be retrieved from discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param str category_id: The identifier number of the category to retreive.
    :param str expected_category_title: The category title that must match
           the specified category_id.
    """
    discourse = Discourse(**mock_request.ro_args())
    categories = discourse.get_categories()
    assert categories is not None
    assert len(categories) > 0
    assert len(categories) == len(mock_request._categories)
    assert categories[0] == (0, 'general')

def test_get_topics(mock_request):
    """Checks that topics can be retrieved from discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    """
    discourse = Discourse(**mock_request.ro_args())
    topics = discourse.get_topics(flag='latest')
    assert topics is not None
    assert len(topics) > 0
    assert len(topics) == len(mock_request._topics)
    assert topics[0] == (0, 'topic-a')

@pytest.mark.parametrize('topic_id, expected_body', [
    (0, 'body-a'),
    (1, 'body-b'),
    (2, 'body-c'),
    ])
def test_get_topic(mock_request, topic_id, expected_body):
    """Checks that a specific topic can be retrieved from discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param str topic_id: The identifier number of the topic to retrieve.
    """
    discourse = Discourse(**mock_request.ro_args())
    topic = discourse.get_topic(topic_id)
    assert topic is not None
    assert type(topic) is dict
    assert 'body' in topic.keys()
    assert topic['body'] == expected_body

@pytest.mark.parametrize('topic_id, expected_num_posts', [
    (None, 6)
    ])
def test_get_posts(mock_request, topic_id, expected_num_posts):
    """Checks that posts can be retrieved from discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param str topic_id: The ID for the topic object to retrieve.
    :param int expected_num_posts: The topic must have this count of posts.
    """
    discourse = Discourse(**mock_request.ro_args())
    posts = discourse.get_posts(topic_id)
    assert posts is not None
    assert len(posts) == expected_num_posts

@pytest.mark.parametrize('post_id, expected_body', [
    (0, 'body-0'),
    (1, 'body-1'),
    (2, 'body-2'),
    ])
def test_get_post(mock_request, post_id, expected_body):
    """Checks that a specific post can be retrieved from discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param str post_id: The ID of the post object to retrieve.
    :param str expected_body: The text that must be present in the post.
    """
    discourse = Discourse(**mock_request.ro_args())
    post = discourse.get_post(post_id)
    assert post is not None
    assert type(post) is dict
    assert 'body' in post.keys()
    assert post['body'] == expected_body

@pytest.mark.parametrize('title,text', [
    ('foo', 'bar'),
    ('Ubuntu Server team update - 16 September 2019',
     "Hi everyone, below you will find the updates of the Ubuntu Server team members from the last week. If you are interested in discussing a topic please start a thread in the Server area5 of this Discourse site.")
    ])
def test_create_topic(mock_request, title, text):
    """Checks that new topics can be added to discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param str title: A name for the topic.
    :param str text: Descriptive body for the topic.
    """
    discourse = Discourse(**mock_request.rw_args())
    next_id = len(mock_request._topics) + 1

    assert discourse.create_topic(title, text) == next_id

    # Verify the topic was created with the required elements
    topic = mock_request.topic_by_id(next_id)
    assert topic['name'] == title
    assert topic['body'] == text

@pytest.mark.parametrize('title,text,topic_id', [
    ('test', 'test', 1),
    ('Post Title', 'Body text', 1),
    ('test', 'test', 1),
    ])
def test_create_post(mock_request, title, text, topic_id):
    """Tests that posts can be created in discourse

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param str title: Title for the post.  While settable, this is not
           publicly visible in Discourse.
    :param int topic_id: (Optional) Discourse topic to associate.  Posts
           are typically displayed under collections organized by topic.
    """
    discourse = Discourse(**mock_request.rw_args())
    next_id = len(mock_request._posts) + 1

    # Create the post
    assert discourse.create_post(title, text, topic_id) == next_id

    # Verify the post was created with the required elements
    post = mock_request.post_by_id(next_id)
    assert post['name'] == title
    assert post['body'] == text
    assert post['topic_id'] == topic_id

@pytest.mark.parametrize('post_id, text', [
    (1, 'Updated text for post'),
    ])
def test_update_post(mock_request, post_id, text):
    """Modifies an existing post to have a different body

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param int post_id: Discourse post to update.
    :param str text: New text for the post's body
    """
    discourse = Discourse(**mock_request.rw_args())
    assert discourse.update_post(post_id, text=text, reason="testing") == True

@pytest.mark.parametrize('post_id', [
    (1),
    (2),
    ])
def test_lock_post(mock_request, post_id):
    """Checks that posts can be locked

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param int post_id: Discourse post to lock.
    """
    discourse = Discourse(**mock_request.rw_args())

    # Test that an unlocked post becomes locked
    assert mock_request.post_by_id(post_id)['locked'] == False
    assert discourse.lock_post(post_id) == True
    assert mock_request.post_by_id(post_id)['locked'] == True

    # Test that a locked post remains locked
    assert discourse.lock_post(post_id) == True
    assert mock_request.post_by_id(post_id)['locked'] == True

@pytest.mark.parametrize('post_id, expected_exception', [
    (100, IndexError),
    ])
def test_lock_post_exceptions(mock_request, post_id, expected_exception):
    """Checks error conditions for invalid locks

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param int post_id: Discourse post to lock.
    """
    discourse = Discourse(**mock_request.rw_args())
    with pytest.raises(expected_exception):
        discourse.lock_post(post_id)

@pytest.mark.parametrize('post_id', [
    (1),
    (2),
    ])
def test_unlock_post(mock_request, post_id):
    """Checks that posts can be unlocked

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param int post_id: The Discourse ID of the post to unlock.
    """
    # Pre-lock the post
    mock_request.post_by_id(post_id)['locked'] = True
    discourse = Discourse(**mock_request.rw_args())
    assert mock_request.post_by_id(post_id)['locked'] == True

    # Test that a locked post becomes unlocked
    assert discourse.unlock_post(post_id) == True
    assert mock_request.post_by_id(post_id)['locked'] == False

    # Test that an unlocked post remains unlocked
    assert discourse.unlock_post(post_id) == True
    assert mock_request.post_by_id(post_id)['locked'] == False

@pytest.mark.parametrize('post_id, expected_exception', [
    (100, IndexError),
    ])
def test_unlock_post_exceptions(mock_request, post_id, expected_exception):
    """Checks error conditions for invalid unlocks

    :param MockRequest mock_request: Fixture providing an artificial
           request() handler.
    :param int post_id: Discourse post ID to unlock.
    """
    discourse = Discourse(**mock_request.rw_args())
    with pytest.raises(expected_exception):
        discourse.unlock_post(post_id)
