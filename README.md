# Post-Discourse

This tool provides a CLI way to interact with the Discourse forum
software.  Creating new topics and posts is a primary function, but
support is included for displaying and looking up items as well.

This is not intended to provide a full mapping of all of Discourse's
REST API.  In particular, this is not intended to be an administrative
tool.

See INSTALL.md for installation directions.


## Configuration

Posting to Discourse requires read-write permissions, which for
Discourse is granted via an "API key".  The administrator of the given
Discourse interest can create and assign API keys per-user.

Once you've gotten an API key, specify it in the config.yml file:

  $ cat ~/.config/post-discourse/config.yml
  username:    john_doe
  api_base:    http://discourse.my-domain.com:3000
  api_key:     abcd1234abcd1234abcd1234abcd1234


## Usage Examples

Create a new toptic
  $ post-discourse --add-topic --title "The title needs to be a complete, unique sentence"
  > The body text must be larger than Discourse's minimum size requirement.
  > ^D
  Created topic #36

Post a status update to topic ID #3:
  $ post-discourse --topic 36 --add-post
  > Text for my post to topic 36
  > ^D
  Created post #37

List topics in the Discourse server
  $ post-discourse --topics
  7 welcome-to-discourse
  15 the-title-needs-to-be-a-complete-unique-sentence
  12 ubuntu-server-announcements

List a given topic's posts:
  $ post-discourse --topic 7 --posts
  10 system

Usage information can be obtained the usual way:
  $ post-discourse --help


## Testing

To perform a quick lint-style validation of the package, run:

  $ python3 setup.py check

The testsuite can be executed in the current environment using:

  $ sudo apt-get install python3-pytest
  $ py.test-3

Alternatively, the testsuite can be made to run into a
pseudo-virtualenv.  This will pull in missing dependencies as eggs.

  $ python3 setup.py test


## Packaging

  $ python3 setup.py sdist
