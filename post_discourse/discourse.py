# -*- Mode: Python; coding: utf-8; indent-tabs-mode: nil; tab-width: 4 -*-
#
# Copyright (C) 2011-2012 Bryce Harrington <bryce@canonical.com>
#
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
#
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import requests

class Discourse(object):
    def __init__(self, api_base, api_key=None, username=None,
                 _get_func=requests.get, _post_func=requests.post):
        """Creates the Discourse object

        :param str api_base: The service's base URL for all API calls.
        :param str api_key: Authorization key from Discourse.
        :param str username: Registered Discourse username to use for operations.
        """
        # n.b. _get_func and _post_func are internal parameters for testing only
        assert api_base

        self._api_base = api_base
        self._api_key = api_key
        self._api_username = username
        self._get_func = _get_func
        self._post_func = _post_func

    def _get(self, api_path, api_args=None):
        """GETs data from given API in JSON format.

        :param str api_path: The relative path for the API call.
        :param str api_args: String of arguments to append to the URL.
        :rtype: JSON text.
        :returns: Content of the request.
        """
        if not api_path:
            return None

        url = '/'.join([self._api_base, api_path])
        if api_args:
            url = '?'.join([url, api_args])

        r = self._get_func(url)
        r.raise_for_status()
        response = r.json()
        return response

    def _post(self, api_path, api_args=None, data=None):
        """POSTs data to given API, returning result in JSON format.

        :param str api_path: The relative path for the API call.
        :param str api_args: String of arguments to append to the URL.
        :param dict data: Parameters to send in the POST operation.
        :rtype: JSON text
        :returns: Content of the request.
        """
        if not api_path:
            return None

        url = '/'.join([self._api_base, api_path])
        headers = {
            'Content-Type': 'multipart/form-data;',
            'Api-Key': self._api_key,
            'Api-Username': self._api_username,
            }

        # TODO: Catch 403 errors (forbidden)
        r = self._post_func(url=url, headers=headers, data=data)
        r.raise_for_status()
        response = r.json()
        return response

    ########################################################################
    ### Read-only operations (no authentication needed)

    def get_categories(self):
        """Retrieves a list of category names available in the Discourse instance.

        :rtype: list of tuples of str pairs
        :returns: List of category names as (id,slug) tuples, or None on error
        """
        # https://discourse.example.com/categories.json
        data = self._get('categories.json')
        if (not data
            or type(data) is not dict
            or 'category_list' not in data.keys()
            or 'categories' not in data['category_list'].keys()):
            return None

        results = []
        for category in data['category_list']['categories']:
            results.append( (category.get('id', -1), category.get('slug', None)) )
        return results

    def get_topics(self, category_id=None, flag=None):
        """Retrieves list of available topics.

        :param str category_id: If specified, return topics for just
            this category.
        :param str flag: Return a subset of the top or most recently
            updated topics across all categories.  Supported values are
            'latest', 'top', 'top-yearly', 'top-quarterly', 'top-monthly',
            and 'top-daily'.
        :rtype: list of tuples of str pairs
        :returns: List of topic names as (id,slug) tuples, or None on error
        """
        data = None
        if category_id:
            # https://discourse.example.com/c/{id}.json
            data = self._get('c/{}.json'.format(category_id))
        elif flag == 'latest':
            # https://discourse.example.com/latest.json
            data = self._get('latest.json')
        elif flag == 'top':
            # https://discourse.example.com/top.json
            data = self._get('top.json')
        elif flag in ['top-yearly', 'top-quarterly', 'top-monthly', 'top-weekly', 'top-daily']:
            # https://discourse.example.com/top/{flag}.json
            data = self._get('top/{}.json'.format(flag[4:]))

        if (not data
            or type(data) is not dict
            or 'topic_list' not in data.keys()
            or 'topics' not in data['topic_list'].keys()):
            return None

        results = []
        for topic in data['topic_list']['topics']:
            results.append((topic.get('id', -1), topic.get('slug', None)))
        return results

    def get_topic(self, topic_id):
        """Retrieves list of available topics.

        :param str topic_id: The Discourse topic ID.
        :rtype: dict
        :returns: The topic data as a dict object.
        """
        # https://discourse.example.com/t/{id}.json
        return self._get('t/{}.json'.format(topic_id))

    def get_posts(self, topic_id=None):
        """Retrieves posts for a given topic, or all latest posts.

        :param str topic_id: The Discourse topic ID, or None to return latests posts.
        :rtype: list of dicts
        :returns: Collection of posts, or None on error
        """
        if topic_id is None:
            # https://discourse.example.com/posts.json
            data = self._get('posts.json')
        else:
            # https://discourse.example.com/t/{id}/posts.json?post_ids=...
            # + Example: %3Fposts_ids%5B%5D%3D12%26post_ids%5B%5D%3D13%26post_ids%5B%5D%3D14
            data = self._get('t/{}/posts.json'.format(topic_id))

        if (not data
            or type(data) is not dict
            or 'post_stream' not in data.keys()
            or 'posts' not in data['post_stream'].keys()):
            return None

        results = []
        for post in data['post_stream']['posts']:
            results.append((post.get('id', -1), post.get('name', None)))
        return results

    def get_post(self, post_id):
        """Retrieves an individual post by its Discourse ID.

        :param str post_id: The Discourse ID of the post.
        :rtype: dict
        :returns: The post data as a dict object.
        """
        # https://discourse.example.com/posts/{id}.json
        return self._get('posts/{}.json'.format(post_id))

    ########################################################################
    ### Read-write operations (authentication required)

    def create_topic(self, title, text, category_id=None):
        """Adds a new topic to Discourse

        :param str title: The visible description of the topic.
        :param str text: Body text for the new topic.
        :param str category_id: The Discourse ID of the category.
        :rtype: int
        :returns: The Discourse ID number of the new topic, or None on error.
        """
        # https://discourse.example.com/posts.json
        # {
        #     "title": "string",
        #     "topic_id": 0,
        #     "raw": "string",
        #     "category": 0,
        #     "target_usernames": "discourse1,discourse2",
        #     "archetype": "private_message",
        #     "created_at": "2017-01-31"
        # }
        data = {
            'title': title,
            'raw':   text
        }
        if category_id:
            data['category'] = category_id
        try:
            result = self._post("posts.json", data=data)
        except:
            return None

        return result.get('id', None)

    def create_post(self, title, text, topic_id):
        """Adds a new post to Discourse

        :param str title: A descriptive name for the post (unused).
        :param str text: The body text of the post.
        :param str topic_id: The Discourse ID of the topic this post belongs to.
        :rtype: int
        :returns: The Discourse ID of the new post.
        """
        # TODO: Should topic_id be required?
        # https://discourse.example.com/posts.json
        # {
        #     "title": "string",
        #     "topic_id": 0,
        #     "raw": "string",
        # }
        data = {
            "title": title,
            "topic_id": int(topic_id),
            "raw": text
            }
        try:
            result = self._post("posts.json", data=data)
        except:
            return None

        return result.get('id', None)

    def update_post(self, post_id, text, reason=None):
        """Changes a post's text in Discourse.

        :param str post_id: The post's Discourse ID.
        :param str text: The new text for the post.
        :param str reason: The (optional) reason for the edit.
        :rtype: bool
        :returns: True if the post was successfully created, False on error.
        """
        # https://discourse.example.com/posts/{id}.json
        # {
        #     "post[raw]": "string",
        #     "post[raw_old]": "string",
        #     "post[edit_reason]": "string",
        #     "post[cooked]": "string"
        # }
        data = {
            'raw': text,
            'edit_reason': reason
        }
        try:
            result = self._post("posts/{}.json".format(post_id), data=data)
        except:
            return False

        return result is not None

    def lock_post(self, post_id):
        """Prevents a post from being edited.

        :param str post_id: The post's Discourse ID.
        :rtype: bool
        :returns: True if the post was successfully locked, False on error.
        """
        # https://discourse.example.com/posts/{id}/locked
        # {
        #     "locked": true
        # }
        data = {
            'locked': True
        }
        try:
            result = self._post("posts/{}/locked".format(post_id), data=data)
        except IndexError:
            raise
        except:
            return False

        return result is not None

    def unlock_post(self, post_id):
        """Allows a locked post to be edited once again.

        :param str post_id: The post's Discourse ID.
        :rtype: bool
        :returns: True if the post was successfully unlocked, False on error.
        """
        # https://discourse.example.com/posts/{id}/locked
        # {
        #     "locked": false
        # }
        data = {
            'locked': False
        }
        try:
            result = self._post("posts/{}/locked".format(post_id), data=data)
        except IndexError:
            raise
        except:
            return False

        return result is not None
