## Installation

The main pre-requisite you might need to install is the YAML python
module.  On Ubuntu 20.04, this can be done like this:

  $ sudo apt-get install python3-setuptools
  $ sudo apt-get install python3-ruamel.yaml

After that, installation follows the usual Python convention:

  $ python ./setup.py check
  $ python ./setup.py build
  $ sudo python3 ./setup.py install

See README.md and post-discourse --help for further directions on how to
configure and use the tool.



